import { UPDATE_LIST_ITEMS } from './actionTypes';

const defaultState = {
    items: []
};

export default (state = defaultState, action) => {
    switch(action.type){
        case UPDATE_LIST_ITEMS:
            return {
                ...state,
                items: action.payload
            };
        default:
            return state;
    }
}