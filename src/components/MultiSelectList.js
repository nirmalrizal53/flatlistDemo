import React, { Component } from 'react';
import { Text, FlatList, TouchableOpacity, View } from "react-native";
import { connect } from 'react-redux';

import { updateListItems } from "../store/actions";

class MyListItem extends React.PureComponent {
    _onPress = () => {
      this.props.onPressItem(this.props.id);
    };
  
    render() {
      return (
        <TouchableOpacity onPress={this._onPress}>
          <View style={styles.itemStyle}>
            <Text>
              {this.props.title}
            </Text>
          </View>
          
        </TouchableOpacity>
      );
    }
  }
  
  class MultiSelectList extends React.PureComponent {
    state = {selected: []};
  
    componentWillMount(){
      const { updateListItems } = this.props;
      const itemsFromApi = [
        {
            id: 1,
            title: 'Item 1'
        },
        {
            id: 2,
            title: 'Item 2'
        },
        {
            id: 3,
            title: 'Item 3'
        },
        {
            id: 4,
            title: 'Item 4'
        },
        {
            id: 5,
            title: 'Item 5'
        }
      ];
      // Call data fetching API above and update the store with data
      updateListItems(itemsFromApi);
    }

    _keyExtractor = (item, index) => String(item.id);
  
    _onPressItem = (id) => {
      const { items, updateListItems } = this.props;
      const newsList = items.filter(a => a.id != id); // Removed clicked item from list based on the id

      // Call this action after getting reponse from API to remove list item
      updateListItems(newsList);
    };
  
    _renderItem = ({item}) => (
      <MyListItem
        id={item.id}
        onPressItem={this._onPressItem}
        selected={false}
        title={item.title}
      />
    );
  
    render() {
      return (
        <FlatList
          style={styles.viewContainer}
          data={this.props.items}
          extraData={this.state}
          keyExtractor={this._keyExtractor}
          renderItem={this._renderItem}
        />
      );
    }
  }

const styles = {
  itemStyle: {
    padding: 20,
    backgroundColor: 'lightblue',
    margin: 20
  },
  viewContainer: {
    marginTop: 40
  }
}

const mapStateToProps = state => {
    return {
        items: state.items
    }
}

export default connect(mapStateToProps, { updateListItems })(MultiSelectList);